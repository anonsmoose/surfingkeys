addSearchAliasX('w', 'Wikipedia', 'https://en.wikipedia.org/wiki/Special:Search/', 'o');
addSearchAliasX('a', 'Arch Wiki', 'https://wiki.archlinux.org/?search=', 'o');
addSearchAliasX('s', 'SearX', 'https://searx.doomer.gq/', 'o');
mapkey('ow', '#8Open Search with alias w', function() {
    Front.openOmnibar({type: "SearchEngine", extra: "w"});
});
mapkey('oa', '#8Open Search with alias a', function() {
    Front.openOmnibar({type: "SearchEngine", extra: "a"});
});
settings.theme = `
.sk_theme {
    font-family: Input Sans Condensed, Charcoal, sans-serif;
    font-size: 10pt;
    background: #282828;
    color: #ebdbb2;
}
.sk_theme tbody {
    color: #b8bb26;
}
.sk_theme input {
    color: #d9dce0;
}
.sk_theme .url {
    color: #98971a;
}
.sk_theme .annotation {
    color: #b16286;
}
.sk_theme .omnibar_highlight {
    color: #ebdbb2;
}
.sk_theme #sk_omnibarSearchResult ul li:nth-child(odd) {
    background: #282828;
}
.sk_theme #sk_omnibarSearchResult ul li.focused {
    background: #d3869b;
}
#sk_status, #sk_find {
    font-size: 20pt;
}`;
